use clipboard::ClipboardContext;
use clipboard::ClipboardProvider;

use anyhow::Result;
use std::io;

fn main() -> Result<()> {
    let mut buffer = "first".to_string();
    while !buffer.is_empty() {
        buffer = String::new();
        io::stdin().read_line(&mut buffer)?;
        buffer = buffer.trim().to_string();
        let output = match unicode_names2::character(&buffer) {
            Some(s) => s,
            None => continue,
        };
        let hex = format!("U{:04X}", output as u32);
        println!("Unicode: {hex}");

        let mut ctx: ClipboardContext = ClipboardProvider::new().unwrap();
        ctx.set_contents(hex).unwrap();
    }
    Ok(())
}
